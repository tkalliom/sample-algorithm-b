import sys

import cv2
import numpy as np
import json

# The analyzer is started once per video to analyze.
# You can set up per-video state here and/or when the first frame is received

firstline = True
video_id = None
video_directory = None


for line in iter(sys.stdin.readline, ''):
    if firstline:
        video_id, video_directory = line.strip().split("\t")
        firstline = False
    else:
        frameline = line.strip()
        
        frame_info = dict()

        field = None
        for token in frameline.split("\t"):
            if field == None:
                field = token
            else:
                frame_info[field] = token
                field = None

        bitmap = np.memmap(video_directory + "/" + frame_info["filename"], dtype='uint8', mode='r', shape=(int(frame_info["height"]), int(frame_info["width"]), 3))
        # Now we have the frame and can work on it
        # cv2.AnalysisTool(settings).run(bitmap)

        # Because we have specified the current-frame dependency on registration, we can assume the file exists
        a_result = None
        with open(video_directory + "/" + frame_info["filename"].replace(".dat", ".sample-algorithm-a.json"), "r") as a_file:
            a_data = json.load(a_file)
            a_result = a_data[0]
        results = '[{"timestamp": ' + frame_info["pts"] +', "score": ' + str(a_result["score"] * 0.5) + ', "rect": {"x": 15, "y": 30, "width": 30, "height": 20}, "classification": {"vocabulary": "terms", "label": "abc"}}]'

        output_path = video_directory + "/" + frame_info["filename"].replace(".dat", ".sample-algorithm-b.json")
        with open(output_path, "w") as output_file:
            output_file.write(results)

        print("Frame analyzed") # content does not matter (at the moment) but the printing of a line marks the frame analysis as finished
        sys.stdout.flush()

# If there are some results you can only calculate after receiving the whole video, you can do so here. The timestamps do not need to correspond to the input.
output_path = video_directory + "/eof.sample-algorithm-b.json"
with open(output_path, "w") as output_file:
    output_file.write("[]") # even if for some request there are no results to provide, at least the empty array must be there

print("Video analyzed")
sys.stdout.flush()

# If you need to clean up algorithm-internal files after a video pass is done, do it here
