/*  This is a sample file defining algorithm information.
    Replace the values with the correct ones.
*/

const algorithm = {
    name: "sample-algorithm-b",
    version: 1,
    port: process.env.PORT || 3001,
    current_frame_dependencies: ["sample-algorithm-a:1"] // "version must be N or greater"
};

const program = {
    command: "python3", // interpreter or binary
    args: ["analyze.py"] //may be left empty
}

const config = {
    algorithm: algorithm,
    program: program
}

module.exports = config;
